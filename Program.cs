﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expresiones_Lambda
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Crear un vector de n números enteros y crear una fúncion con expresión lambda que permita insertar los números colocados 
               por consola y mostrar el vector resultante. */

            {
                //Creación del vector.
                int[] vector;
                Console.WriteLine("Ingresa la cantidad de numeros enteros que se agregaran al vector");
                int n = int.Parse(Console.ReadLine());
                vector = new int[n];

                //Aqui la expresion lambda va a recibir un entero y lo que hace es ingresar numeros entero a un vector y mostrarlos.
                Action<int> Ingresar = (numero) => 
                {
                    for (int i = 0; i < numero; i++)
                    {
                        Console.Write("Ingresa un número entero al vector: ");
                        vector[i] = int.Parse(Console.ReadLine());
                    }
                    Console.WriteLine("Números Ingresado con éxito");
                    Console.WriteLine("***********");
                    Console.WriteLine("Números en el vector");
                    for (int i = 0; i < numero; i++)
                    {

                        Console.Write("[" + vector[i] + "]");
                    }
                };
                //Se llama a la expresión lambda. 
                Ingresar(n);
                Console.WriteLine("\numero");
            }
        }
    }
}